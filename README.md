# Quiz 2

This project has as purpose solve the following three questions about **OOP** in Java.

**1. How does Java implement polymorphism? Give a code example.**  
**Polymorphism** is a concept related to **OOP**.  
Polymorphism is the interaction between classes, methods and objects.
And, as result it shows that there are many ways to carry out a method or action depending on the object behavior.

**2. How does Java implement inheritance? Give example.**  
**Inheritance** is a **OOP** concept.  
Inheritance refers to the process where a class acquires the properties from another classes within the project.

**3. What is try... catch in Java? Give example.**  
**Try...catch** is a handle exception statement commonly used while programming.  
* **Try** statement will test the code founded within this block of code.  
* **Catch** statement will process the errors founded in the try statement by error exceptions given within this catch block.
