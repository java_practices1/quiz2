/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgtry.pkgcatch;
import java.util.Scanner;

/**
 * Try...catch is a handle exception statement commonly used while programming.
 * Try statement will test the code founded within this block of code.
 * Catch statement will process the errors founded in the try statement by error exceptions given within this catch block.
 */

/**
 *
 * @author Sandra Vasquez
 */
public class TryCatch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        int divide=0;
        
        System.out.println("Divide 2 numbers");
        System.out.println("Fisrt number: ");
        int first = scanner.nextInt();
        System.out.println("Second number: ");
        int second = scanner.nextInt();
        
        try{
            divide = first/second;
            System.out.println("Result: " + divide);
        }
        catch (Exception e){
            System.out.println(first+" cannot be divided by "+second);
        }
        
    }
    
}
