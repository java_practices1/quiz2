/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inheritance;


    /**
     * Inheritance is a OOP concept.
     * Inheritance refers to the process where a class acquires the properties
     * from another classes within the project.
     */

    /**Lets declare a Students class,
     * where there are two more classes, Girls and Boys
     * and a superclass called data.
    */  

/**
 *
 * @author Sandra Vasquez
 */
public class Students {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Give values to name and age variables
        Girl g = new Girl("Paola", 20);
        Boy b = new Boy("Carlos", 18);
        
        //Print girl information
        g.getName();
        g.getAge();
        
        //Print boy information
        b.getName();
        b.getAge();
    }
    
}

// Super class, gets name and age of Students.
class Data{
    // Declafre variables name and age
    String name;
    int age;
    
    Data(String name, int age){
        this.name = name;
        this.age = age;
    }
    
    
    public void getName(){
        System.out.println("Student's name: "+name);
    }
    
    public void getAge(){
        System.out.println("Student's age: "+age);
    }
}

// Sub class "Girl" to extract data
public class Girl extends Data{
    Girl(String name, int age){
        super(name, age); //Inheritance of Data class to Girl class
    }
}

// Sub class "Boy" to extract data
public class Boy extends Data{
    Boy(String name, int age){
        super(name, age);//Inheritance of Data Class to Boy class
    }
}
