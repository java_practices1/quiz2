/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polymorphism;

/**
 *
 * @author Sandra Vasquez
 */
public class Polymorphism {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        Plant p;
        
        p = new Vine();
        p.plantProduces();
        
        p = new OrangeTree();
        p.plantProduces();
        
        p = new CoconutPalm();
        p.plantProduces();
    }
    
}

    class Plant{
        public void plantProduces(){
            System.out.println("Plant produces...");
        }
    }
    
    class Vine extends Plant{
        public void plantProduces(){
            System.out.println("Vine produces grapes.");
        }
    }
    
    class OrangeTree extends Plant{
        public void plantProduces(){
            System.out.println("Orange tree produces sweet oranges.");
        }
    }
    
    class CoconutPalm extends Plant{
        public void plantProduces(){
            System.out.println("Coconut palm produces coconuts.");
        }
    }
